var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var mongoose = require('mongoose');
var ejs = require('ejs');
var app = express();

Async = require('async');
Axios = require('axios');
Moment = require('moment')

const user = require('./routes/user');
const api = require('./routes/api');

Response = require('./services/response');
HelperService = require('./services/helperService');
ConstantService = require('./services/constantService');


_ = require('lodash');
var Schema = mongoose.Schema;
ObjectId = mongoose.Types.ObjectId;

mongoose.connect(require('./models/connection-string'), {useNewUrlParser: true, useCreateIndex : true,  useUnifiedTopology: true}, function(err, f){
	require('./models/all-models').toContext(global);
})

app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: false , limit:'5mb'}));
app.use(cookieParser());


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');



app.use(express.static(path.join(__dirname, 'public')));

app.use('/user', user);
app.use('/api', api);




  app.listen(3000, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:3000')
  })

