import { action, observable } from 'mobx'
import { useStaticRendering } from 'mobx-react'

const isServer = typeof window === 'undefined'
// eslint-disable-next-line react-hooks/rules-of-hooks
useStaticRendering(isServer)

export class Store {
  @observable lastUpdate = 0
  @observable light = false
  @observable openLoginModal = false
  @observable blogs = []
  @observable bookingDetails = {}
  @observable userDetail = {}
  @observable loginCallbackFunction
  @observable seoText = {
      title : "",
      description : "",
      ogTitle : "",
      ogImage : "",
      ogDescription : "",
      ogUrl : ""
  }

  hydrate(serializedStore) {
    this.lastUpdate =
      serializedStore.lastUpdate != null
        ? serializedStore.lastUpdate
        : Date.now()
    this.light = !!serializedStore.light
  }

  @action setSeoText = (seo) => {
      this.seoText.title = seo.title ? seo.title + " - Kiki Tips" : "Kiki Tips - Slam Book Link Creator!"
      this.seoText.description = seo.ogDescription ? seo.ogDescription : "Create slam book link and send it to your friends, it's free!"

      this.seoText.ogUrl = "https://www.kiki.tips/" + (seo.ogUrl ? seo.ogUrl : "")
      this.seoText.ogTitle = seo.ogTitle ? seo.ogTitle + " - Kiki Tips" : "Kiki Tips - Slam Book Link Creator!"
      this.seoText.ogImage = seo.ogImage ? seo.ogImage : "https://kiki.tips/assets/img/b3.jpeg"
      this.seoText.ogImage2 = seo.ogImage2 ? seo.ogImage2 : "https://kiki.tips/assets/img/b3.jpeg"
      this.seoText.ogDescription = seo.ogDescription ? seo.ogDescription : "Create slam book link and send it to your friends"
  }

  @action toogleLoginModal = () => {
      this.openLoginModal = !this.openLoginModal
  }

  @action openLoginModalWithCallback = (loginCallback) => {
      this.openLoginModal = !this.openLoginModal
      this.loginCallbackFunction = loginCallback
  }

  @action updateBookingDetails = (handle, bookingTime) => {
      this.bookingDetails = {handle : handle, bookingTime : bookingTime}
  }

  @action updateUserDetail = () => {
      let name = window.localStorage.getItem('name')
      let accessToken = window.localStorage.getItem('accessToken')
      let profilePic = window.localStorage.getItem('profilePic')
      let phoneNumber = window.localStorage.getItem('phoneNumber')
      this.openLoginModal = false
      this.userDetail = {name : name, accessToken : accessToken, profilePic : profilePic, phoneNumber : phoneNumber}
      if(this.loginCallbackFunction)this.loginCallbackFunction()
  }

  @action start = () => {
    this.timer = setInterval(() => {
      this.lastUpdate = Date.now()
      this.light = true
    }, 1000)
  }

  stop = () => clearInterval(this.timer)
}

export async function fetchInitialStoreState() {
  // You can do anything to fetch initial store state

  return {}
}
