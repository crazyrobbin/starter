module.exports = {

    taxMultiplierForPrice: function(tax){
    	return (1 + tax/100);
    },

    parseDateForAndroid: function(date){
        // yyyy-MM-dd HH:mm:ss
        return Moment(date).format('YYYY-MM-DD HH:mm:ss')
    },

    randomInt : function(low, high) {
        return Math.floor(Math.random() * (high - low) + low)
    },

    taxMultiplierForTax: function(tax){
    	return (tax/100);
    },

    addOneSecToTime: function(date){
        return new Date(date.getTime()+10)
    },

    getTodayStart: function() {
        var today = new Date();
        return new Date(today.getFullYear(), today.getMonth(), today.getDate());
    },

    getDayStart: function(date) {
        var date2 = new Date(date)
        return new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
    },

    getDayEnd: function(date) {
        var date2 = new Date(date)
        return new Date(date2.getFullYear(), date2.getMonth(), date2.getDate(), 23, 59, 59, 999);
    },

    getStartDate: function(d) {
        var today = new Date(d);
        return new Date(today.getFullYear(), today.getMonth(), today.getDate());
    },

    getEndDate: function(d) {
        var today = new Date(d);
        return new Date(today.getFullYear(), today.getMonth(), today.getDate(), 23, 59, 59, 999);
    },

    getNoOfDaysInMonth: function(year, month) {
        var lastDay = new Date(2017, month + 1, 0, 23, 59, 59);
        console.log(lastDay);
        return lastDay.getDate();
    },

    getTodayEnd: function() {
        var today = new Date();
        return new Date(today.getFullYear(), today.getMonth(), today.getDate(), 23, 59, 59, 999);
    },

    getMonth: function(index) {
        var months = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return months[index];
    },

    getDateDifferenceInDay: function(date1, date2){
        var diffMs = (date1.getTime() - date2.getTime());
        var diffDays = Math.round(diffMs / 86400000); // days
        return diffDays
    },

    getLastDayStart: function(noOfDay) {
        var days = noOfDay; // Days you want to subtract
        var date = new Date();
        var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
        return new Date(last.getTime());
    },

    getDateInString: function(datetime) {
        return datetime.getDate() + " " + HelperService.getMonth(datetime.getMonth()) + " " + datetime.getFullYear()
    },

    getDateTimeInStringFormat: function(date) {
        var datetime = new Date(date)
        var hours = datetime.getHours();
      var minutes = datetime.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
        return datetime.getDate() + " " + HelperService.getMonth(datetime.getMonth()) + " " + strTime
    },

    getTimeInString: function(date) {
        var datetime = new Date(date)
        var hours = datetime.getHours();
      var minutes = datetime.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime
    },


    getDateTimeInString: function(datetime){
        var d = new Date();
        var diffMs = (d.getTime() - datetime.getTime());
        var diffDays = Math.round(diffMs / 86400000); // days
        var diffHrs = Math.round((diffMs % 86400000) / 3600000); // hours
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        if(diffDays>=1){
            return "on " + datetime.getDate() + " " + HelperService.getMonth(datetime.getMonth());
        }else if(diffHrs >= 1){
            return diffHrs + " hr ago";
        }else{
            return diffMins + " mins ago";
        }
    },

    getDistanceBtwCordinates: function(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
        var dLon = this.deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return parseInt(d);
    },

    deg2rad: function(deg) {
        return deg * (Math.PI / 180);
    },

    parseUrlMetaTags: function(d){
        console.log(d.ogImage)
    	return {
    		title : d.ogTitle,
    		description : d.ogDescription,
    		websiteName : d.ogSiteName,
    		url : d.ogUrl,
    		imageUrl : Array.isArray(d.ogImage) && d.ogImage.length > 0 ? d.ogImage[0].url : d.ogImage.url
    	};
    },

    getUrlForVideoImage: function(p){
        return p.url+"?w=700&h=700&mode=fill&fill=solid&fill-color="+p.color.replace('#','')
    },

    getSMSUrlForOtp: function(messageId, message, phoneNumbers, type, otp, retry) {
        var param = {},
            request = "";
        param.authkey = "164034A93iTmJcMu595dd01d";
        param.mobile = "91" + phoneNumbers.toString();
        param.message = message.replace(/&/g, "%26");
        param.otp = otp;
        param.sender = "BEUSLN";
        for (var key in param) {
            request += key + "=" + encodeURI(param[key]) + "&";
        }
        request = encodeURI(request.substr(0, request.length - 1));
        var retryUrl = "https://control.msg91.com/api/retryotp.php?authkey=164034A93iTmJcMu595dd01d&mobile=91" + phoneNumbers.toString() + "&retrytype=" + retry;
        var realUrl = !retry ? ("https://control.msg91.com/api/sendotp.php?" + request) : retryUrl;
        console.log(realUrl);
        return realUrl;
    },


};
