module.exports = {

    getAttractionTitle: function(type){
    	let d = ["Top Attractions", "Parks & Garden", "Museums & Halls", "City Market", "Religious Sites", "Other Attractions"]
    	if(type == -1)return d
    	return d[type-1]
    },

    getThingsToDoTitle: function(type){
    	let d = ["What to See", "Exhibitions", "Shows", "Activities"]
    	if(type == -1)return d
    	return d[type-1]
    },

    getLevelsPointsBreakdown: function(type){
        return [1,200, 500, 800, 1200, 1800, 2500, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000, 16000, 17000, 18000, 20000]
    },

    getUserBadge: function(level){
        let badge = "Traveller Badge - None"
        if(level > 19)badge = "Guru Traveller"
        else if(level > 12)badge = "Expert Traveller"
        else if(level > 9)badge = "Explorer Traveller"
        else if(level > 5)badge = "Nomad Traveller"
        else if(level > 2)badge = "Rookie Traveller"
        return badge
    },

    getNewAttraction: function(){
        return []
    },

    getAppointmentStatus: function(staus){
        if(staus == 1)return "Call Pending"
        if(staus == 2)return "Appointment Cancelled"
        if(staus == 3)return "Call Completed"
        return ""
    },

    getOtherAttractionType: function(type){
        let obj = {
            name : "Restaurant",
            attractionId : "1",
            points : 0,
            description : "Selected location is a restaurant/ Dhabba ..",
            distance : 0
        }
        if(type == 1)return obj
        else if(type == 2){
            obj.name = "Market"
            obj.attractionId = "2"
            obj.description = "Selected location is a market"
            return obj
        }else if(type == 3){
            obj.name = "Scenic"
            obj.attractionId = "3"
            obj.description = "Great landscape view from this location"
            return obj
        }/*else if(type == 4){
            obj.name = "Attraction/ Monument"
            obj.attractionId = "4"
            obj.description = ""
            return obj
        }*/else if(type == 5){
            obj.name = "Others"
            obj.attractionId = "5"
            obj.description = "Others"
            return obj
        }else return obj
    },

    getTopDestinations: function(latitude, longitude, currentCityId){
        let data = [
            {
                name : "Delhi",
                cityId : "5c728231ec8b92214c108b28",
                image : "https://botinsti-cdn.sirv.com/Delhi/india-712575_1920.jpg",
                distance : HelperService.getDistanceBtwCordinates(28.612512, 77.148115, latitude, longitude)
            },
            {
                name : "Manali",
                cityId : "5d728cc10cc1ebf931e51f61",
                image : "https://botinsti.sirv.com/Himachal%20Pradesh/raghav-yadav-lPmuLHB1QBM-unsplash.jpg",
                distance : HelperService.getDistanceBtwCordinates(32.2422, 77.187073, latitude, longitude)
            },
            {
                name : "Agra",
                cityId : "5c7282ddb941fd15dcc22f25",
                image : "https://botinsti-cdn.sirv.com/UP/Agra/pexels-photo-1603650.jpeg",
                distance : HelperService.getDistanceBtwCordinates(27.187424, 78.008867, latitude, longitude)
            },
            {
                name : "Jaipur",
                cityId : "5c72c0ee7732b81fe0a1767a",
                image : "https://botinsti-cdn.sirv.com/Rajasthan/Jaipur/palace-of-winds-326523_1280.jpg",
                distance : HelperService.getDistanceBtwCordinates(26.910495, 75.78647, latitude, longitude)
            },
            {
                name : "Jaisalmer",
                cityId : "5d361d53e609387541c96368",
                image : "https://botinsti.sirv.com/Rajasthan/Jaisalmer/bada-bagh-3181803_1280.jpg",
                distance : HelperService.getDistanceBtwCordinates(26.917831, 70.915399, latitude, longitude)
            }
        ]
        return _.filter(data, function(d){ return d.cityId != currentCityId + ""})
    },



};
