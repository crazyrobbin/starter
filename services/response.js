module.exports = {

    parse: function(error, messageOrError){
        if(error)return {success : false, message : messageOrError, result : {}};
        else if(typeof messageOrError == 'string')return {success : true, message : messageOrError};
        else return { success : true, message : "Data Found", result : { data : [messageOrError], total : 1}};
    },
};
