var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'));
}


var newSchema = new Schema({
  
  'name': { type: String, required : true },
  'phoneNumber': { type: String, unique: true, sparse: true},
  'profilePic': { type: String, default : "" },
  'emailId': { type: String, unique: true, sparse: true},
  'info': { type: String, default : "" },
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function(next){
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});


let crypto = require('crypto')
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var secret = 'asdsdasdsfskFFGFGdasdsdaFGDGFGDFGFDg46545sdfshjgpoanvbg'

module.exports = mongoose.model('User', newSchema);


module.exports.findUser = function(userId, cb){
    User.findOne({_id : userId}, {name : 1, profilePic : 1, level : 1},function(err, user){
        return cb({
            name : user.name,
            userId : user.id,
            profilePic : user.profilePic,
            level : user.level
        })
    })
}

module.exports.findUserSimpleObj = async function(userId){
    let user = await User.findOne({_id : userId}, {name : 1, profilePic : 1, level : 1})
    return new Promise(function(resolve, reject){
        resolve({
            name : user.name,
            userId : user.id,
            profilePic : user.profilePic,
            level : user.level
        })
    })

}

module.exports.parseForLogin = function(user) {
    const payload = {
        userId: user.id.toString()  
    };
    var token = jwt.sign(payload, secret, {
        expiresIn: 86400000 // expires in 24 hours*1000
    });

    return{
        userId : user.id,
        name : user.name,
        phoneNumber : user.phoneNumber,
        profilePic : user.profilePic,
        handle : user.handle,
        token : token
    };
};



module.exports.decodePassword = function(password) {
  let hash = crypto.createHash('sha256');
  hash.update(password);
  return hash.digest('hex').toUpperCase();
};

module.exports.getNewUserForEmailObj = function(req) {
  let hash = crypto.createHash('sha256');
  hash.update(req.body.password);
  let password = hash.digest('hex').toUpperCase();
    return{
        name : req.body.name,
        emailId : req.body.emailId,
        password : password,
        profilePic : "https://botinsti.sirv.com/default_profile.jpg",
    };
};


module.exports.getNewUserForFacebookObj = function(data) {
  let hash = crypto.createHash('sha256');
  hash.update(data.id+Math.random());
  let password = hash.digest('hex').toUpperCase();
    return{
        name : data.name,
        emailId : data.email ? data.email : "",
        facebookId : data.id,
        profilePic : "https://graph.facebook.com/" + data.id + "/picture?type=large",
        password : password,
    };
};

module.exports.getNewUserForGoogleObj = function(data) {
  let hash = crypto.createHash('sha256');
  hash.update(data.sub + Math.random());
  let password = hash.digest('hex').toUpperCase();
    return{
        name : data.name,
        emailId : data.email ? data.email : "",
        googleId : data.sub,
        profilePic : data.picture,
        password : password,
    };
};