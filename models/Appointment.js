var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'));
}


var newSchema = new Schema({
  
  'userName': { type: String },
  'userId': { type: Schema.Types.ObjectId, ref: '' },
  'influencerId': { type: Schema.Types.ObjectId, ref: '' },
  'influencerName': { type: String },
  'influencerProfilePic': { type: String },
  'appointmentTime': { type: Date },
  'status': { type: Number, default : 0 }, // 0 - created, 1 - booked, 2 - cancelled, 3 - completed
  'payableAmount': { type: Number, default : 0 }, 
  'influencerRate': { type: Number, default : 0 },
  'influencerCallMin': { type: Number, default : 0 },
  'discount': { type: Number, default : 0 },
  'subtotal': { type: Number, default : 0 },
  'actualCallTime': { type: Date },
  'razorPayCaptureResponse': {},
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function(next){
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});



module.exports = mongoose.model('Appointment', newSchema);



module.exports.parseNewObj = function(a){
    return{
        appointmentId : a.id,
        userName : a.userName,
        influencerName : a.influencerName,
        influencerProfilePic : a.influencerProfilePic,
        appointmentTime : HelperService.getDateTimeInStringFormat(a.appointmentTime),
        payableAmount : a.payableAmount,
    }
}


module.exports.parseForAppointmentList = function(a, currentUserId){
    let type = currentUserId + "" == a.userId + "" ? 1 : 2 // 1 - influcencer appointment, 2 - normal people appointment
    return{
        appointmentId : a.id,
        userName : type == 1 ? a.influencerName : a.userName,
        profilePic : type == 1 ? a.influencerProfilePic : "",
        appointmentTime : HelperService.getDateTimeInStringFormat(a.appointmentTime),
        showCallLink : true,
        status : ConstantService.getAppointmentStatus(a.status)
    }
}


module.exports.captureRazorPayPayment = async function(req){
      var Razorpay = require('razorpay');
      var instance = new Razorpay({
          // key_id: "rzp_test_ce6MjDUu9PVnWo",
          // key_secret: "XeDfCYSWj9MmRE2do7wdznv2"
          key_id: "rzp_live_W43MRCY6KVEKin",
          key_secret: "wUtGlJ7HJkqOwLU6bT2vODL1"
      });
      
      // rzp_live_W43MRCY6KVEKin
      // wUtGlJ7HJkqOwLU6bT2vODL1

    let razorPayObj = await instance.payments.fetch(req.body.razorPayId)
    if (razorPayObj && razorPayObj.notes.appointmentId) {
        let appointment = await Appointment.findOne({_id : razorPayObj.notes.appointmentId, status: { $in: [0] }})
        if(appointment){
            if(appointment.payableAmount * 100 == req.body.amount){
                try{
                    let resObj = await instance.payments.capture(req.body.razorPayId, req.body.amount)
                    await Appointment.updateOne({_id : appointment.id, status : 0}, {status : 1, razorPayCaptureResponse : resObj})
                    return {success : true, message: "Payment Success Booked"}
                }catch(err){
                  console.log(err)
                    return {success : false, message: "Unable to capture payment, contact customer care!"}
                }
                Appointment.capturePayment(instance, appointment, req)
            }
            
        }else{
            return {success : false, message: "Invalid appointment, contact customer care!"}
        }
    }else{
        return {success : false, message: "Invalid payment, contact customer care!"}
    }
}

