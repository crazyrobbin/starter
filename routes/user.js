var express = require('express');
var router = express.Router();


router.post('/loginWithEmail', function(req, res){
	User.findOne({emailId : req.body.emailId}, function(Er, user){
		if(user){
			if(user.password == User.decodePassword(req.body.password)){
      			res.json(Response.parse(false, User.parseForLogin(user)));
			}else{
      			res.json(Response.parse(true, 'Invalid Password'));
			}
		}else{
      		res.json(Response.parse(true, 'Email Id not Registered'));
		}
	})
});


router.post('/loginWithOtp', async(req, res) => {
    let otps = await Otp.find({phoneNumber : req.body.phoneNumber, otp : req.body.otp, used : 0})
    if(otps.length > 0){
          await Otp.updateMany({phoneNumber : req.body.phoneNumber}, {used : 1})
          let user = await User.findOne({phoneNumber : req.body.phoneNumber})
          if(user){
              res.json(Response.parse(false, User.parseForLogin(user)))
          }else{
              let newUserObj = {name : "user", phoneNumber : req.body.phoneNumber, profilePic : "https://a.wordpress.com/avatar/unknown-128.jpg"}
              let u = await User.create(newUserObj)
              res.json(Response.parse(false, User.parseForLogin(u)))
          }
    }else{
        res.json(Response.parse(true, 'Invalid Otp'));
    }
});

router.post('/sendOtp', async(req, res) => {
    var otp = Math.floor(Math.random() * 9000) + 1000;
    let phoneNumber = req.body.phoneNumber
    if (phoneNumber == "9695748822" || phoneNumber == "9898989898" ) otp = 1234;
    otp = 1234
    await Otp.create({ otp: otp, phoneNumber: phoneNumber })
    // var url = HelperService.getSmsUrlForOtp('BEUSLN', "Your OTP for Kiki Tips is " + otp, [phoneNumber], 'T', otp, 0);
    res.json(Response.parse(false, 'Otp Sent'));
});

router.post('/registerWithEmail', function(req, res){
  if(!req.body.emailId || req.body.emailId == ''){
      res.json(Response.parse(true, 'Invalid Email Id'));
  }else{
      User.findOne({emailId : req.body.emailId}, function(Er, user){
          if(user){
                res.json(Response.parse(true, 'Email Id Already Registered'));
          }else{
            if(req.body.name && req.body.emailId && req.body.password && req.body.name != '' && req.body.emailId != '' && req.body.password != ''){
              User.create(User.getNewUserForEmailObj(req), function(err, user){
                    res.json(Response.parse(false, User.parseForLogin(user)));
              });
            }else{
                  res.json(Response.parse(true, 'Email id, name, password required'));
            }
          }
      });
  }
  
});

// https://graph.facebook.com/me?access_token=EAADKTet7l0IBAAX36yQbStjoZCPwq8sKebx5iZBFq8icqGzMHj1muotrVLgFhs4J29ZAn4PQhdrIVhyu98sBkuuZAGIVYVxZCWlRMPb5V2ZAuFsuZAVNDME1BEk8HFYVegATUSqhJnDyFtJGZCHy4dU7eZANd5zsXfrjVwfQQZCJ4c4mc3XwFNv1CPoMwMuuqpv0cpqCIhzBvfRwZDZD&fields=id,name,email

router.post('/loginWithFacebook', function(req, res){
	let url = "https://graph.facebook.com/me?access_token="+req.body.accessToken+"&fields=id,name,email"
	Axios.get(url)
  	.then(function (response) {
  		let data = response.data;
  		if(data){
  			User.findOne({facebookId : data.id}, function(Er, user){
  				if(user){
	      			res.json(Response.parse(false, User.parseForLogin(user)));
  				}else{
            let newObj = User.getNewUserForFacebookObj(data)
  					User.create(newObj, function(err, newUser){
                if(err){
                    User.update({emailId : data.email}, {facebookId : data.id}, function(er, d){
                      User.findOne({emailId : data.email}, {id : 1, name : 1, profilePic : 1, handle : 1, instagramId : 1}, function(err, user){
                          res.json(Response.parse(false, User.parseForLogin(user)));
                      })
                    })
                }else{
                      res.json(Response.parse(false, User.parseForLogin(newUser)));
                }
					});
  				}
  			});
  		}else{
		    res.json(Response.parse(true, 'Invalid access Token'));
  		}
  	})
  	.catch(function (error) {
	    res.json(Response.parse(true, 'Invalid access Token'));
  	});
});

// '' + accessToken


router.post('/loginWithGoogle', function(req, res){
	let url = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="+req.body.accessToken
	Axios.get(url)
  	.then(function (response) {
  		let data = response.data;
  		if(data){
  			User.findOne({googleId : data.sub}, function(Er, user){
  				if(user){
	      			res.json(Response.parse(false, User.parseForLogin(user)));
  				}else{
            let newObj = User.getNewUserForGoogleObj(data)
  					User.create(newObj, function(err, newUser){
                if(err){
                    User.update({emailId : newObj.emailId}, {googleId : newObj.googleId}, function(er, d){
                      User.findOne({emailId : newObj.emailId}, {id : 1, name : 1, profilePic : 1, handle: 1, instagramId : 1}, function(err, user){
                          res.json(Response.parse(false, User.parseForLogin(user)));
                      })
                    })
                }else{
                      res.json(Response.parse(false, User.parseForLogin(newUser)));
                }
					});
  				}
  			});
  		}else{
		    res.json(Response.parse(true, 'Invalid access Token'));
  		}
  	})
  	.catch(function (error) {
	    res.json(Response.parse(true, 'Invalid access Token'));
  	});
});


module.exports = router;